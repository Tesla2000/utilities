import inspect


class LazyInit(type):
  def __new__(cls, name, bases, namespace):
    init_parameters = tuple(inspect.signature(namespace['__init__']).parameters.keys())[1:]
    defaults = {}
    for parameter in init_parameters:
      default = inspect.signature(namespace['__init__']).parameters[parameter].default
      if not isinstance(default, inspect._empty):
        defaults[parameter] = default
    def init(self, *args, **kwargs):
      for key, value in defaults.items():
        self.__setattr__(key, value)
      for param, arg in zip(init_parameters, args):
        self.__setattr__(param, arg)
      for key, value in kwargs.items():
        self.__setattr__(key, value)
    namespace['__init__'] = init
    return super().__new__(cls, name, bases, namespace)