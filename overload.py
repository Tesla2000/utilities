from collections import defaultdict
overloaded = {}


def overload(*types):
  def inner(f):
    enchanced_f = (f.__qualname__, types)
    overloaded[enchanced_f] = f

    def wrapped(*args, **kwargs):
      types = tuple(map(type, args))
      enchanced_f = [f.__qualname__, types]
      if '.' in f.__qualname__:
        enchanced_f = enchanced_f[:-1] + [enchanced_f[-1][1:]]
      return overloaded[tuple(enchanced_f)](*args, **kwargs)

    return wrapped

  return inner


def setter(prep, k, v, supSetter):
  if callable(v):
    def wrap(*args):
      f = prep.d[k][len(args)]
      if isinstance(f, int): raise AttributeError()
      return f(*args)

    prep.d[k][v.__code__.co_argcount] = v
    v = wrap
  supSetter(k, v)


class Prep(dict):
  def __init__(self):
    self.d = defaultdict(lambda: defaultdict(int))

  def __setitem__(self, k, v):
    setter(self, k, v, super().__setitem__)


class Overloadable(type):
  @classmethod
  def __prepare__(cls, *args, **kwds): return Prep()

  def __new__(metacls, name, bases, prep, **kwargs):
    prep['_Meta__DCT'] = prep
    return super().__new__(metacls, name, bases, prep, **kwargs)

  def __setattr__(self, k, v):
    setter(self.__DCT, k, v, super().__setattr__)
