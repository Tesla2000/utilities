import json
import os.path
import datetime


class StateSaver:
  dt = datetime

  def __init__(self, save_folder_path=None, file_name=None):
    self._save_folder_path = save_folder_path
    self._name = file_name

  def _get_defaults(self, scope, save_name, save_folder_path, load=False):
    if scope is None:
      scope = globals()
    if save_folder_path is None:
      save_folder_path = self._save_folder_path
    if save_folder_path is None:
      save_folder_path = 'saves'
    if not os.path.isdir(save_folder_path):
      os.makedirs(save_folder_path)
    if save_name is None:
      save_name = self._name
    if save_name is None:
      save_name = str(StateSaver.dt.datetime.now())
      if load:
        valid_files = tuple(filter(lambda filename: filename.endswith('.json'), os.listdir(save_folder_path)))
        if not valid_files:
          raise ValueError(f"There are not .json saves in folder {save_folder_path}. "
                           f"Save state or change lookup folder!")
        save_name = max(os.listdir(save_folder_path))
    save_name = save_name.rstrip('.json') + '.json'
    return scope, save_name, save_folder_path

  @staticmethod
  def _is_serializable(obj):
    try:
      json.dumps(obj)
      return True
    except (TypeError, OverflowError):
      return False

  def map_to_new_dict(self, variables):
    new_variables = {}
    for key, value in variables.items():
      if isinstance(value, type(StateSaver.dt)) and value.__name__ == 'random':
        new_variables[key] = {'__random__': True, 'state': value.getstate()}
      if key in ('__module__', '__doc__', '__classname__'):
        continue
      if (callable(value) and hasattr(value, '__code__')) or isinstance(value, type(StateSaver.dt)):
        continue
      if hasattr(value, '__dict__'):
        new_variables[key] = self.map_to_new_dict(value.__dict__)
        new_variables[key]['__classname__'] = str(type(value).__name__)
        continue
      new_variables[key] = value
      if not self._is_serializable(value):
        del new_variables[key]
    return new_variables

  @staticmethod
  def load_to_dict(load_to: dict, load_from: dict):
    immutable_fields = []
    for key, value in load_from.items():
      if hasattr(load_to.get(key), '__dict__'):
        if isinstance(value, dict) and value.get('__random__'):
          value['state'] = (value['state'][0], tuple(value['state'][1]), value['state'][2])
          load_to[key].setstate(value['state'])
        unchanged = StateSaver.load_to_dict(load_to[key].__dict__, value)
        for field in unchanged:
          setattr(load_to[key], field, value[field])
        continue
      if isinstance(value, dict):
        if '__classname__' in value:
          classname = value.pop('__classname__')
          class_ = load_to[classname]
          load_to[key] = class_.__new__(class_)
          load_to[key].__dict__ = value
          continue
        if key not in load_to:
          load_to[key] = {}
        StateSaver.load_to_dict(load_to[key], value)
        continue
      if key == '__classname__':
        continue
      try:
        load_to[key] = value
      except TypeError:
        immutable_fields.append(key)
    return immutable_fields

  def save(self, file_name=None, scope=None, save_folder_path=None):
    variables, file_name, save_folder_path = self._get_defaults(scope, file_name, save_folder_path)
    new_variables = self.map_to_new_dict(variables)
    with open(os.path.join(save_folder_path, file_name), 'w') as file:
      json.dump(new_variables, file, indent=2)

  def load(self, file_name=None, scope=None, save_folder_path=None):
    scope, file_name, save_folder_path = self._get_defaults(scope, file_name, save_folder_path, load=True)
    print(os.path.join(save_folder_path, file_name))
    with open(os.path.join(save_folder_path, file_name)) as file:
      saved_variables = json.load(file)
    self.load_to_dict(scope, saved_variables)
